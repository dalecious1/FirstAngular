angular.module('Main')
	.directive('footer', function() {
		return {
			restrict: 'EA',
			replace: true,
			scope: true,
			templateUrl: 'src/main/templates/footer.html',
			controller: 'FooterController'
		};
	}
);