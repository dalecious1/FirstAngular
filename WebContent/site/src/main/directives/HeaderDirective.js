angular.module('Main')
	.directive('header', function() {
		return {
			restrict: 'EA',
			replace: true,
			scope: true,
			templateUrl: 'src/main/templates/header.html',
			controller: 'HeaderController'
		};
	});