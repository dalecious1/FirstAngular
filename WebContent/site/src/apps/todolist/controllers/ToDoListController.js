angular.module('ToDo')
	.controller('ToDoListController', ['ToDoService', '$log', function(ToDoService, $log) {
		var todolist = this;
		
		todolist.currentToDoItem = null;
		todolist.editedItem = {};
		todolist.inprogress = [];
		todolist.completed = [];
		
		todolist.setCurrentItem = function(toDoItem) {
			todolist.currentToDoItem = toDoItem;
			todolist.editedItem = angular.copy(todolist.currentToDoItem);
		};
		
		todolist.inprogress = {
				"name": "Laundry",
				"description": "I need more clothes",
				"status": "In Progress"
		}, {
				"name": "Grocery Shopping",
				"description": "Or else I'll be hungry",
				"status": "In Progress"
		};
		
		todolist.completed = {
				"name": "Panel Interview",
				"description": "Now I'm on market",
				"status": "Finished"
		}, {
				"name": "Go to work",
				"description": "So I can get paid",
				"status": "Finished"
		};
		
	}]);










