angular.module('MadLib')
	.controller('MadLibController1', ['StoryService', function(StoryService) {
		var madlibCtrl = this;
		
		madlibCtrl.update = function(story) {
			StoryService.update = angular.copy(story);
		};
		
		madlibCtrl.reset = function() {
			$scope.story = angular.copy($scope.master);
		};
		
		madlibCtrl.reset();
	}]
);