angular.module('MadLib')
	.controller('MadLibController2', ['$scope', 'StoryService', function($scope, StoryService) {
		$scope.master = {};
		
		$scope.update = function(story) {
			$scope.master = angular.copy(story);
		};
		
		$scope.reset = function() {
			$scope.story = angular.copy($scope.master);
		};
		
		$scope.reset();
	}]
);