angular.module('Lunch')
	.factory('LunchService', ['$resource', '$http', '$log', function($resource, $http, $log) {
		
		var service = {};
		
		var message = "In the lunch service scope";
		var lunches = [];
		var cleanLunches = [];
		
		service.setMessage = function(_message) {
			message = _message;
		};
		
		service.getMessage = function() {
			return message;
		};
		
		service.setLunches = function(_lunches) {
			lunches = _lunches;
		};
		
		service.getLunches = function() {
			return lunches;
		};
		
		// AngularJS calling Spring MVC REST url
		// specify the search term and sort in the URL itself because it's a GET request
		service.getCleanLunches = function(term, sort) {
			$http.get('http://localhost:7001/FirstAngular/rest/lunch?term=' + term + '&sort=' + sort)
				.success(function(data, status) {
					lunches = data;
					
					// removes all old values from cleanLunches
					cleanLunches.splice(0,100);
					
					lunches.forEach(function(restaurant) {
						
						// storing only relevant information into an array that we're gonna print

						cleanLunches.push({
							restaurantId: restaurant.id,
							restaurantName: restaurant.name,
							restaurantRating: restaurant.rating_img_url,
							restaurantUrl: restaurant.url,
							restaurantLocation: restaurant.location.display_address
						});
						
					});
				})
				.error(function() {
					alert("Yelp API is down");
				});
			
			return cleanLunches;
		};
		
		service.getRandom = function() {
			cleanLunches = service.getCleanLunches(term, sort);
			
			var randomRestaurant = {};
			
			var randomInt = Math.ceil(Math.random()*9);
			
			randomRestaurant = {
					id: cleanLunches[randomInt].restaurantId,
					name: cleanLunches[randomInt].name,
					rating: cleanLunches[randomInt].rating_img_url,
					url: cleanLunches[randomInt].url,
					location: cleanLunches[randomInt].location.display_address
			};
			
			return randomRestaurant;
		};
		
		service.newRandomRestaurant = function() {
			
			
			return randomRestaurant;
		};
			
	return service;
	}]);