angular.module('Lunch')
.controller('LunchController', ['$http', '$log', 'LunchService', function($http, $log, LunchService) {	
	this.message = "Testing form input";
	
	this.term = "";
	this.sort = "";
	this.lunches = {};
	this.randomRestaurant = {};
	this.message = "In Lunch Controller scope";
	this.text = "";
	this.address = "";
	this.areResultsEmpty = true;
	
	setLunches = function(_lunches) {
		this.lunches = _lunches;
	};
	getLunches = function() {
		return this.lunches;
	};
	
	setRandomLunch = function(obj) {
		this.randomRestaurant = obj;
	};
	getRandomLunch = function() {
		return this.randomRestaurant;
	};
	
	this.lookForLunch = function(term, sort) {
		this.clear();
		this.lunches = LunchService.getCleanLunches(term, sort);
		this.isLunchesEmpty = false;
//		this.randomRestaurant = {
//				name: this.lunches[randomInt].restaurantName,
//				rating: this.lunches[randomInt].restaurantRating,
//				url: this.lunches[randomInt].restaurantUrl,
//				location: this.lunches[randomInt].restaurantLocation
//		};
	};
	
	this.reset = function() {
		this.term = null;
		this.sort = null;
	};
	
	this.clear = function() {
		var table = document.getElementById('lunch-table');
		while(table.rows.length > 0) {
			table.deleteRow(0);
		}
	};
	
//	this.randomLunch = function() {
//		if (isArrayGenerated == true) {
//			var randomInt = Math.ceil(Math.random()*9);
//			this.randomRestaurant = this.lunches[randomInt];
//			
//			if (isGeneratedRandom == true) {
//				var div = document.createElement('div');
//				
//				div.className = 'row';
//				
//				div.innerHTML = '<h3>Looking for a random restaurant?<br/> <small>Try this:</small></h3>\
//				<p>\
//					{{lunch.randomRestaurant.restaurantName}}<br/>\
//					<img ng-src="{{ restaurant.restaurantRating }}"><br/>\
//					{{lunch.randomRestaurant.restaurantLocation}}\
//				</p>';
//				
//				document.getElementById('#random').appendChild(div);
//			}
//			
//			isGeneratedRandom = true;
//		}
//		else {
//			alert("You need to search for some restaurants first");
//		}
//	};
	
	this.randomRestaurant = LunchService.getRandom();
	
	this.newRandomRestaurant = function() { 
		randomRestaurant = LunchService.newRandomRestaurant();
	};
	
}]);