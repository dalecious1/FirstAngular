angular.module('Dale', ['FirstAngular']);

// here I've created my own module called 'Dale'
// When you declare a route in the main application FirstAngular.js,
// Angular will look for 'Dale' when it's specified as a javascript parameter in the main module
// Look at FirstAngular.js in the home directory and you should see FirstAngular.js