// Here is the main struture of Angular
/*
 * First you need to specify which module this controller is part of
 * Next I've used the $scope service to add a string to the controller.
 * First you need to include $scope as a dependency in the function you pass into the controller
 * And then in the html file you can access the message in double curly braces {{[Your Name].message}}
 * 
 * Create your own html file in the location specified under the templateUrl attribute in the .config function in FirstAngular.js
 */

angular.module('Dale')
	.controller('DaleController', ['$scope', function($scope) {
		$scope.message = "You've successfully achieved injection";
	}]
);