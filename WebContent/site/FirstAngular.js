// What is going on here?
/*
 * The mainApp is the main module of our application.
 * In the square brackets, we will tell the mainApp that we also have included these submodules
 * ngRoute is an Angular module
 * while Dale is a custom submodule that we will create
 * */

var mainApp = angular.module('FirstAngular', [
	'ngRoute',
	'ngResource',
	'Snake',
	'Main',
	'Dale',
	'ToDo',
	'Vernacular',
	'Lunch',
	'MadLib',
	'Calculator'
	]);

// Config
/*
 * Here we're using the ngRoute Angular module we included above
 * Before we can even get this far, we've included the angular-route.min.js script file
 * Next we use the $routeProvider object and specify it as a dependency in our .config function
 * 
 * In the .config function, we're passing an array of elements.
 * The first elements need to be Strings; '$routeProvider' is the logical name
 * We also pass the actual object $routeProvider in the function parameter
 * 
 * */

mainApp.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
	$routeProvider
		// -------------------------- root/main/default
		.when('/', {
			templateUrl: 'src/main/templates/main.html',
			controller: 'MainController',
			controllerAs: 'main'
		})
		.when('/main', {
			templateUrl: 'src/main/templates/main.html',
			controller: 'MainController',
			controllerAs: 'main'
		})
		// -------------------------- apps
		.when('/snake', {
			templateUrl: 'src/apps/snake/snake.html',
			controller: 'SnakeController',
			controllerAs: 'snake'
		})
		.when('/todolist', {
			templateUrl: 'src/apps/todolist/templates/todolist.html',
			controller: 'ToDoListController',
			controllerAs: 'todolist'
		})
		.when('/madlibs', {
			templateUrl: 'src/apps/madlibs/templates/init.html',
			controller: 'MadLibController1',
			controllerAs: 'madlib'
		})
		.when('/madlibs/story1', {
			templateUrl: 'src/apps/madlibs/templates/story1.html',
			controller: 'MadLibController1',
			controllerAs: 'madlib'
		})
		.when('/madlibs/story2', {
			templateUrl: 'src/apps/madlibs/templates/story2.html',
			controller: 'MadLibController2',
			controllerAs: 'madlib'
		})
		.when('/madlibs/story3', {
			templateUrl: 'src/apps/madlibs/templates/story3.html',
			controller: 'MadLibController3',
			controllerAs: 'madlib'
		})
		.when('/weather/home', {
			templateUrl: 'src/apps/weather/templates/weather.html',
			controller: 'WeatherHomeController',
			controllerAs: 'weatherhome'
		})
		.when('/weather/forecast', {
			templateUrl: 'src/apps/weather/templates/forecast.html',
			controller: 'ForecastController',
			controllerAs: 'forecast'
		})
		.when('/lunch', {
			templateUrl: 'src/apps/lunch/templates/lunch.html',
			controller: 'LunchController as lunch'
		})
		.when('/lunch/random', {
			templateUrl: 'src/apps/lunch/templates/random.html',
			controller: 'LunchController as lunch'
		})
		// -------------------------- lessons
		.when('/mvc', {
			templateUrl: 'src/lessons/templates/mvc.html',
			controller: 'LessonsController',
			controllerAs: 'lessons'
		})
		.when('/modules', {
			templateUrl: 'src/lessons/templates/modules.html',
			controller: 'LessonsController',
			controllerAs: 'lessons'
		})
		.when('/configuration', {
			templateUrl: 'src/lessons/templates/configuration.html',
			controller: 'LessonsController',
			controllerAs: 'lessons'
		})
		.when('/instructions', {
			templateUrl: 'src/lessons/templates/instructions.html',
			controller: 'LessonsController',
			controllerAs: 'lessons'
		})
		// -------------------------------- vernacular
		.when('/structures', {
			templateUrl: 'src/vernacular/templates/structures.html',
			controller: 'VernacularController',
			controllerAs: 'vernacular'
		})
		.when('/di', {
			templateUrl: 'src/vernacular/templates/di.html',
			controller: 'VernacularController',
			controllerAs: 'vernacular'
		})
		.when('/services', {
			templateUrl: 'src/vernacular/templates/services.html',
			controller: 'VernacularController',
			controllerAs: 'vernacular'
		})
		// ------------------------------ heroes
		.when('/dale', {
			templateUrl: 'src/heroes/dale/templates/dale.html',
			controller: 'DaleController',
			controllerAs: 'dale'
		})
				
		/*
		 * This is the basic syntax to set up a route in AngularjS
		 * 
		 * There are two mandatory parts to routing: templateUrl and controller
		 * 
		 * TemplateUrl specifies the template html file that we're going to asynchronously load to the home page
		 * Specifically, when you click on the link, the page will load under the HTML element with the "ng-view" attribute/directive
		 * To go from HTML to AngularJS, just rememeber that HTML attributes are AngularJS directives.
		 * 
		 * Templates are base HTML pages that we create that can also asynchronously load HTML/JS
		 * 
		 * We also specify a controller to load with the route.
		 * Much like hardcoding ng-controller="MyController" directive into an HTML page
		 * Specifying the controller tells the browser to use the controller named MyController in this page
		 * 
		 * controllerAs allows us to specify a logical name so there's no confusion between identical elements
		 * It's almost like a namespace/package structure
		 * 
		 * Next, create [Your Name]Controller.js in the src/heroes/controllers
		 * */
		
		// -------------------------------- otherwise; we could redirect to 404
		.otherwise({
			redirectTo: '/'
		});
	
	$httpProvider.defaults.withCredentials = true;
	$httpProvider.defaults.useXDomain= true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
	
	//RestangularProvider.setBaseUrl('http://api.yelp.com/v2/search/?');
}]);

// Added all header request and response.
//mainApp.all('/*', function (request, response, next) {
//    response.header("Access-Control-Allow-Origin", "*");
//    response.header("Access-Control-Allow-Headers", "X-Requested-With");
//    response.header("Access-Control-Allow-Methods", "GET, POST", "PUT", "DELETE");
//    next();
//
//});


