head.load([
	'//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js',
	'//ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.js',
	'//ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular-route.js',
	'//code.angularjs.org/1.4.7/angular-resource.js',

	'src/apps/snake/Snake.js',
	'src/apps/snake/controllers/SnakeController.js',
	'src/apps/snake/js/snake1.js',

	'src/main/Main.js',
	'src/main/controllers/MainController.js',
	'src/main/controllers/FooterController.js',
	'src/main/controllers/HeaderController.js',
	'src/main/directives/FooterDirective.js',
	'src/main/directives/HeaderDirective.js',
	
	'src/lessons/Lessons.js',
	'src/lessons/controllers/LessonsController.js',
	
	'src/heroes/dale/Dale.js',
	'src/heroes/dale/controllers/DaleController.js',
	
	'src/apps/todolist/ToDo.js',
	'src/apps/todolist/services/ToDoService.js',
	'src/apps/todolist/controllers/ToDoListController.js',
	
	'src/vernacular/Vernacular.js',
	'src/vernacular/controllers/VernacularController.js',
	
	'src/apps/madlibs/MadLib.js',
	'src/apps/madlibs/controllers/MadLibController1.js',
	'src/apps/madlibs/controllers/MadLibController2.js',
	'src/apps/madlibs/controllers/MadLibController3.js',
	
	'src/apps/lunch/Lunch.js',
	'src/apps/lunch/services/LunchService.js',
	'src/apps/lunch/controllers/LunchController.js',
	
	'src/apps/calculator/Calculator.js',
	'src/apps/calculator/services/CalculatorService.js',
	'src/apps/calculator/controllers/CalculatorController.js',
	
	'FirstAngular.js'
]);