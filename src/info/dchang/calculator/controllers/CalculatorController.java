package info.dchang.calculator.controllers;

import info.dchang.calculator.services.CalculatorService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {
	
	// calculator example
	
	CalculatorService calculatorService = new CalculatorService();
	
	@RequestMapping(value="/calculator/add", method=RequestMethod.GET)
	public double add(@RequestParam(value="a") double a, @RequestParam(value="b") double b) {
		return calculatorService.add(a,b);
	}
	
	@RequestMapping(value="/calculator/subtract", method=RequestMethod.GET)
	public double subtract(@RequestParam(value="a") double a, @RequestParam(value="b") double b) {
		return calculatorService.subtract(a,b);
	}
	
	@RequestMapping(value="/calculator/multiply", method=RequestMethod.GET)
	public double multiply(@RequestParam(value="a") double a, @RequestParam(value="b") double b) {
		return calculatorService.multiply(a,b);
	}

}
