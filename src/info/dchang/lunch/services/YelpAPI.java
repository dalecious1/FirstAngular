package info.dchang.lunch.services;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

/**
 * Time to eat Yelp!!
 * 
 * @author Dale
 *
 */

public class YelpAPI {
	
	private static final String API_HOST = "api.yelp.com";
	private static final String DEFAULT_LOCATION = "Fairfax, VA";
	private static final int SEARCH_LIMIT = 50;
	private static final String SEARCH_PATH = "/v2/search";
	private static final String BUSINESS_PATH = "/v2/business";
	
	/*
	 * Not technically supposed to reveal this to you guys,
	 * but here are the OAuth credentials that Yelp supplied 
	 * me when I signed up for Yelp's API
	 */
	
	OAuthService service;
	Token accessToken;
	
	/*
	 * Setting up Yelp API OAuth credentials
	 */
	
	public YelpAPI(String consumerKey, String consumerSecret, String token, String tokenSecret) {
			this.service = new ServiceBuilder()
									.provider(TwoStepOAuth.class)
									.apiKey(consumerKey)
									.apiSecret(consumerSecret)
									.build();
			this.accessToken = new Token(token, tokenSecret);
	}
	
	/*
	 * Create and send a request to Yelp's search API w/ certain parameters
	 */
	
	public String searchForBusinessesByLocation(String term, String location, int sort) {
		OAuthRequest request = createOAuthRequest(SEARCH_PATH);
		request.addQuerystringParameter("term", term);
		request.addQuerystringParameter("location", location);
		request.addQuerystringParameter("limit", String.valueOf(SEARCH_LIMIT));
		request.addQuerystringParameter("sort", String.valueOf(sort));
		return sendRequestAndGetResponse(request);
	}
	
	// get a single business
	public String searchByBusinessId(String businessID) {
		OAuthRequest request = createOAuthRequest(BUSINESS_PATH + "/" + businessID);
		return sendRequestAndGetResponse(request);
	}
	
	private OAuthRequest createOAuthRequest(String path) {
		OAuthRequest request = new OAuthRequest(Verb.GET, "https://" + API_HOST + path);
		return request;
	}
	
	private String sendRequestAndGetResponse(OAuthRequest request) {
		System.out.println("Querying " + request.getCompleteUrl() + " ...");
		this.service.signRequest(this.accessToken, request);
		Response response = request.send();
		return response.getBody();
	}
	
	// retrieve 10 results from yelp
	public JSONArray queryAPI(YelpAPI yelpApi, String term, int sort) {
		String searchResponseJSON = yelpApi.searchForBusinessesByLocation(term, DEFAULT_LOCATION, sort);
		
		JSONParser parser = new JSONParser();
		JSONObject response = null;
		
		try {
			response = (JSONObject) parser.parse(searchResponseJSON);
		} 
		catch (ParseException pe) {
			System.out.println("Error: could not parse JSON reponse:");
			System.out.println(searchResponseJSON);
			System.exit(1);
		}
		
		JSONArray businesses = (JSONArray) response.get("businesses");
		
		return businesses; 
	}

}