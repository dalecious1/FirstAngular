package net.mv.madlib.controllers;

import net.mv.madlib.domain.Stories;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class MadlibController {
	
	// add values to story object
	@RequestMapping(value="/madlib/new", method=RequestMethod.POST)
	public ModelAndView saveValues(ModelMap modelMap) {
		return new ModelAndView("");
	}
	
}
