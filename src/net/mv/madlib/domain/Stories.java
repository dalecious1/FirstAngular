package net.mv.madlib.domain;

public class Stories {
	
	private String hero;
	private String firstName;
	private String noun1;
	private String noun2;
	private String noun3;
	private String aNoun1;
	private String aNoun2;
	private String pNoun1;
	private String pNoun2;
	private String adj1;
	private String adj2;
	private String adj3;
	private String adj4;
	private String adj5;
	private String adj6;
	private String adj7;
	private String adj8;
	private String adj9;
	private String color1;
	private String color2;
	private String color3;
	private String adv1;
	private String adv2;
	private String adv3;
	private String pastVerb1;
	private String pastVerb2;
	private String pastVerb3;
	private String pastVerb4;
	private String pastVerb5;
	private String pastVerb6;
	private String pastVerb7;
	private String presentVerb7;
	private String animal;
	private String dwelling;
	private String terrain;
	private String location;
	private String bPart1;
	private String bPart2;
	private String bPart3;
	private String timePeriod;
	private String emotion;
	private String clothing;
	private String clothingP1;
	private String clothingP2;
	
	public String getHero() {
		return hero;
	}
	public void setHero(String hero) {
		this.hero = hero;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getNoun1() {
		return noun1;
	}
	public void setNoun1(String noun1) {
		this.noun1 = noun1;
	}
	public String getNoun2() {
		return noun2;
	}
	public void setNoun2(String noun2) {
		this.noun2 = noun2;
	}
	public String getNoun3() {
		return noun3;
	}
	public void setNoun3(String noun3) {
		this.noun3 = noun3;
	}
	public String getaNoun1() {
		return aNoun1;
	}
	public void setaNoun1(String aNoun1) {
		this.aNoun1 = aNoun1;
	}
	public String getaNoun2() {
		return aNoun2;
	}
	public void setaNoun2(String aNoun2) {
		this.aNoun2 = aNoun2;
	}
	public String getpNoun1() {
		return pNoun1;
	}
	public void setpNoun1(String pNoun1) {
		this.pNoun1 = pNoun1;
	}
	public String getpNoun2() {
		return pNoun2;
	}
	public void setpNoun2(String pNoun2) {
		this.pNoun2 = pNoun2;
	}
	public String getAdj1() {
		return adj1;
	}
	public void setAdj1(String adj1) {
		this.adj1 = adj1;
	}
	public String getAdj2() {
		return adj2;
	}
	public void setAdj2(String adj2) {
		this.adj2 = adj2;
	}
	public String getAdj3() {
		return adj3;
	}
	public void setAdj3(String adj3) {
		this.adj3 = adj3;
	}
	public String getAdj4() {
		return adj4;
	}
	public void setAdj4(String adj4) {
		this.adj4 = adj4;
	}
	public String getAdj5() {
		return adj5;
	}
	public void setAdj5(String adj5) {
		this.adj5 = adj5;
	}
	public String getAdj6() {
		return adj6;
	}
	public void setAdj6(String adj6) {
		this.adj6 = adj6;
	}
	public String getAdj7() {
		return adj7;
	}
	public void setAdj7(String adj7) {
		this.adj7 = adj7;
	}
	public String getAdj8() {
		return adj8;
	}
	public void setAdj8(String adj8) {
		this.adj8 = adj8;
	}
	public String getAdj9() {
		return adj9;
	}
	public void setAdj9(String adj9) {
		this.adj9 = adj9;
	}
	public String getColor1() {
		return color1;
	}
	public void setColor1(String color1) {
		this.color1 = color1;
	}
	public String getColor2() {
		return color2;
	}
	public void setColor2(String color2) {
		this.color2 = color2;
	}
	public String getColor3() {
		return color3;
	}
	public void setColor3(String color3) {
		this.color3 = color3;
	}
	public String getAdv1() {
		return adv1;
	}
	public void setAdv1(String adv1) {
		this.adv1 = adv1;
	}
	public String getAdv2() {
		return adv2;
	}
	public void setAdv2(String adv2) {
		this.adv2 = adv2;
	}
	public String getAdv3() {
		return adv3;
	}
	public void setAdv3(String adv3) {
		this.adv3 = adv3;
	}
	public String getPastVerb1() {
		return pastVerb1;
	}
	public void setPastVerb1(String pastVerb1) {
		this.pastVerb1 = pastVerb1;
	}
	public String getPastVerb2() {
		return pastVerb2;
	}
	public void setPastVerb2(String pastVerb2) {
		this.pastVerb2 = pastVerb2;
	}
	public String getPastVerb3() {
		return pastVerb3;
	}
	public void setPastVerb3(String pastVerb3) {
		this.pastVerb3 = pastVerb3;
	}
	public String getPastVerb4() {
		return pastVerb4;
	}
	public void setPastVerb4(String pastVerb4) {
		this.pastVerb4 = pastVerb4;
	}
	public String getPastVerb5() {
		return pastVerb5;
	}
	public void setPastVerb5(String pastVerb5) {
		this.pastVerb5 = pastVerb5;
	}
	public String getPastVerb6() {
		return pastVerb6;
	}
	public void setPastVerb6(String pastVerb6) {
		this.pastVerb6 = pastVerb6;
	}
	public String getPastVerb7() {
		return pastVerb7;
	}
	public void setPastVerb7(String pastVerb7) {
		this.pastVerb7 = pastVerb7;
	}
	public String getPresentVerb7() {
		return presentVerb7;
	}
	public void setPresentVerb7(String presentVerb7) {
		this.presentVerb7 = presentVerb7;
	}
	public String getAnimal() {
		return animal;
	}
	public void setAnimal(String animal) {
		this.animal = animal;
	}
	public String getDwelling() {
		return dwelling;
	}
	public void setDwelling(String dwelling) {
		this.dwelling = dwelling;
	}
	public String getTerrain() {
		return terrain;
	}
	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getbPart1() {
		return bPart1;
	}
	public void setbPart1(String bPart1) {
		this.bPart1 = bPart1;
	}
	public String getbPart2() {
		return bPart2;
	}
	public void setbPart2(String bPart2) {
		this.bPart2 = bPart2;
	}
	public String getbPart3() {
		return bPart3;
	}
	public void setbPart3(String bPart3) {
		this.bPart3 = bPart3;
	}
	public String getTimePeriod() {
		return timePeriod;
	}
	public void setTimePeriod(String timePeriod) {
		this.timePeriod = timePeriod;
	}
	public String getEmotion() {
		return emotion;
	}
	public void setEmotion(String emotion) {
		this.emotion = emotion;
	}
	public String getClothing() {
		return clothing;
	}
	public void setClothing(String clothing) {
		this.clothing = clothing;
	}
	public String getClothingP1() {
		return clothingP1;
	}
	public void setClothingP1(String clothingP1) {
		this.clothingP1 = clothingP1;
	}
	public String getClothingP2() {
		return clothingP2;
	}
	public void setClothingP2(String clothingP2) {
		this.clothingP2 = clothingP2;
	}
	
}
