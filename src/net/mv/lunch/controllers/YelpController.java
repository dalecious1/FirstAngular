package net.mv.lunch.controllers;

import net.mv.lunch.services.YelpAPI;

import org.json.simple.JSONArray;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*
 * 1) Create a controller to take search parameters from the front end
 * 2) Use a back-end Java-based service to consume Yelp's REST service
 * 3) Take information and display on front end
 */

@RestController
public class YelpController {
	
	private final String CONSUMER_KEY = "78xwvu072HO6svufKqPEKA";
	private final String CONSUMER_SECRET = "OsB_jKSwiremOJj-EY2dWIwyTWY";
	private final String TOKEN = "pUmAn0gflBtOqVaWnZ4Gof_bERnQ9W7m";
	private final String TOKEN_SECRET = "jkDFqbeFFiiqS_FhRBwutd_p3L4";
	
	// our endpoint is /rest/lunch
	
	@RequestMapping("/rest/lunch")
	public JSONArray getLunch(@RequestParam String term, @RequestParam int number) {
		YelpAPI ya = new YelpAPI(CONSUMER_KEY, CONSUMER_SECRET, TOKEN, TOKEN_SECRET);
		
		JSONArray results = ya.queryAPI(ya);
		
		return results;
	}

}
