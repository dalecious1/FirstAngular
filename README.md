Now that the master is organized, we can all be on the same page with this project. It's so fitting that we have a secret project for our secret facebook group.

----------

List of things to the main:

+ Fix continuity for all Controller-As syntax (get rid of all $scope)

+ Can't do Yelp, but can use Spring's internal implementation of REST to create REST Controllers to create and consume our own API's

+ Fix to-do list

+ Finish the weather app

+ Fix snake

+ Not add anything new until the current applications are finished